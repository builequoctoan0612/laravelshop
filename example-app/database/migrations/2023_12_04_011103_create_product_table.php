<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('iduser');
            $table->string('name');
            $table->float('price');
            $table->unsignedInteger('id_category');
            $table->unsignedInteger('id_brand');
            $table->tinyInteger('status');
            $table->integer('sale');
            $table->string('company');
            $table->string('hinhanh');
            $table->string('detail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product');
    }
};
