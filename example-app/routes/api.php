<?php

use App\Http\Controllers\Api\AdminController;
use App\Http\Controllers\Api\APIMemberController;
use App\Http\Controllers\Api\APIBlogController;
use App\Http\Controllers\Api\APIProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

    // member
    Route::post('/login',[APIMemberController::class, 'login']);
    Route::post('/register',[APIMemberController::class, 'register']);

    // product
    Route::get('/product',[APIProductController::class, 'productHome']);
    Route::get('/product/list',[APIProductController::class, 'productList']);
   
    Route::get('/product/wishlist',[APIProductController::class, 'productWishlist']);
    Route::get('/product/detail/{id}',[APIProductController::class, 'detail']);
    Route::post('/product/cart',[APIProductController::class, 'productCart']);
    

    // middleware
    Route::middleware(['auth:sanctum','apimember'])->group(function () {
        Route::post('/user/update/{id}',[APIMemberController::class, 'update']);
        
        Route::get('/user/my-product',[APIProductController::class, 'myProduct']);
        Route::post('/user/product/add',[APIProductController::class, 'store']);
        Route::get('/user/product/{id}',[APIProductController::class, 'show']);
        Route::post('/user/product/update/{id}',[APIProductController::class, 'update']);
        Route::get('/user/product/delete/{id}',[APIProductController::class, 'deleteProduct']);
        Route::post('/blog/comment/{id}',[APIBlogController::class, 'comment']);
        Route::post('/blog/rate/{id}',[APIBlogController::class, 'rate']);
        

    });
    // // 
    Route::get('/blog/rate/{id}',[APIBlogController::class, 'rateBlog']);
    Route::get('/category-brand',[APIProductController::class, 'listCategoryBrand']);
   

    //Blog Api
    Route::get('/blog',[APIBlogController::class, 'list']);
    Route::get('/blog/detail/{id}',[APIBlogController::class, 'show']);
    
    Route::get('/blog/detail-pagination/{id}',[APIBlogController::class, 'pagingBlogDetail']);


    //admin
    Route::post('/loginadmin',[AdminController::class, 'login']);
    Route::middleware(['auth:sanctum','apiadmin'])->prefix('/admin')->group(function () {
        Route::post('/update-blog/{id}',[APIBlogController::class, 'update']);
        Route::get('/edit-blog/{id}',[APIBlogController::class, 'edit']);
        

    });
   