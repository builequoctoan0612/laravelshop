<?php


use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\frontend\MemberController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => 'admin'], function () {


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/myadmin', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/profile',[UserController::class, 'index'])->name('user');
Route::post('/profile',[UserController::class, 'update']);

Route::get('/country',[CountryController::class, 'index']);

Route::get('/addcountry',[CountryController::class, 'viewcreate']);
Route::post('/addcountry',[CountryController::class, 'create']);
//xoa
Route::get('/country/{id}',[CountryController::class, 'destroy']);

Route::get('/blog',[BlogController::class, 'index']);

Route::get('/addblog',[BlogController::class, 'viewcreate']);
Route::post('/addblog',[BlogController::class, 'create']);

Route::get('/editblog/{id}',[BlogController::class, 'edit']);
Route::post('/editblog',[BlogController::class, 'update']);

//xoa
Route::get('/blog/{id}',[BlogController::class, 'destroy']);
});




Route::group(['middleware' => 'memberNotLogin'], function () {

    Route::get('/registermb', [MemberController::class,'index']);
    Route::post('/registermb', [MemberController::class,'create']);

    Route::get('/loginmb', [MemberController::class,'indexlog']);
    Route::post('/loginmb', [MemberController::class,'login']);
    
});
Route::group(['middleware' => 'member'], function () {
//blog
//Route::get('/blogmb', [BlogController::class,'index']);
    Route::get('/logoutmb', [MemberController::class,'logout']);
    

    Route::post('/rating', [\App\Http\Controllers\frontend\BlogController::class,'rating'])->name('blog.rating');
    Route::post('/comment', [\App\Http\Controllers\frontend\BlogController::class,'comment'])->name('blog.comment');
    Route::post('/reply', [\App\Http\Controllers\frontend\BlogController::class,'replycomment']);

    //profile
    Route::get('/account/update', [\App\Http\Controllers\frontend\MemberController::class,'viewupdate']);
    Route::post('/account/update',[UserController::class, 'update']);

    Route::get('/account/add-product', [\App\Http\Controllers\frontend\ProductController::class,'viewadd'])->name('addproduct');
    Route::post('/account/add-product', [\App\Http\Controllers\frontend\ProductController::class,'store']);

    Route::get('/account/myproduct', [\App\Http\Controllers\frontend\ProductController::class,'index']);

    Route::get('/account/editproduct/{id}', [\App\Http\Controllers\frontend\ProductController::class,'viewedit']);
    Route::post('/account/editproduct/{id}', [\App\Http\Controllers\frontend\ProductController::class,'edit']);
    //xoa
    Route::get('/account/myproduct/{id}', [\App\Http\Controllers\frontend\ProductController::class,'destroy']);

    Route::get('/test',[\App\Http\Controllers\MailController::class,'index']);

    Route::get('/checkout',[\App\Http\Controllers\frontend\CartController::class,'checkout']);
    Route::post('/checkout',[\App\Http\Controllers\frontend\CartController::class,'sendcheck']);
});
//frontend
Route::get('/index',  [\App\Http\Controllers\frontend\ProductController::class,'viewhome']);
    Route::get('/blogmb', [\App\Http\Controllers\frontend\BlogController::class,'index']);
    Route::get('/blogdetail/{id}', [\App\Http\Controllers\frontend\BlogController::class,'indexdetail']);
 //search
    Route::post('/search',[\App\Http\Controllers\frontend\CartController::class,'search']);
    Route::post('/search-price',[\App\Http\Controllers\frontend\CartController::class,'search_price']);
    Route::get('/searchadvanced',[\App\Http\Controllers\frontend\CartController::class,'viewsearchadvanced']);
    Route::post('/searchadvanced',[\App\Http\Controllers\frontend\CartController::class,'searchadvanced']);


    Route::get('/account/detailproduct/{id}', [\App\Http\Controllers\frontend\ProductController::class,'detail']);
    //ajax mua hang
    Route::post('/add-cart', [\App\Http\Controllers\frontend\CartController::class,'addcart']);

    Route::get('/show-cart', [\App\Http\Controllers\frontend\CartController::class,'showcart']);
    Route::post('/update-qty-cart', [\App\Http\Controllers\frontend\CartController::class,'update']);


