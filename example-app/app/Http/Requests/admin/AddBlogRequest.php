<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddBlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'description' => 'required', 
            'content' => 'required', 
            'image'=>'image|mimes:jpeg,png,jpg,gif|max:2048'          
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'image' => ':attribute phải chọn hình ảnh',
            'max' => ':attribute quá lớn',
            
        ];
    }
    public function attributes()
    {
        return [
            'description' => 'Mô tả', 
            'content' => 'Nội dung', 
            'title' => 'Tên',
        ];
    }
}
