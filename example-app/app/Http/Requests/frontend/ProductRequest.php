<?php

namespace App\Http\Requests\frontend;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name'=>'required',
            'price'=>'required|integer',
            'id_category'=>'required',
            'id_brand'=>'required',
            'detail'=>'required',
            'image' => 'array|max:3',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'integer' => ':attribute phải nhập số',   
            'image' => ':attribute phải chọn hình ảnh',
            'max' => ':attribute chỉ được 3 ảnh < 1mb',        
        ];
    }
    public function attributes()
    {
        return [
            'name' => 'Tên sản phẩm',
            'price' => 'Giá',
            'id_category' => 'Loại',
            'image' => 'Hình ảnh',
            'id_brand' => 'Nhãn hàng',
            'detail'=>'Mô tả',

        ];
    }
}
