<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function login(Request $request)
    {
        $login = [
            'email' => $request->email,
            'password' => $request->password,
            'level' => 1
        ];
        
        $remember = false;
        if ($request->remember_me) {
            $remember = true;
        }
       

        if (Auth::attempt($login, $remember)) {

             $user = Auth::user(); 
             
             $token = $user->createToken('authToken')->plainTextToken;
            return response()->json([
                    'success' => 'success',
                    'token' => $token, 
                    'Auth' => Auth::user()
                ], 
                200
            ); 
        } else {
            return response()->json([
                    'response' => 'error',
                    'errors' => ['errors' => 'invalid email or password'],
                ],
                500); 
        }
    }
    
}