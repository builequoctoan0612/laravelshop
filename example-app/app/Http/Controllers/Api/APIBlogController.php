<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddBlogRequest;
use App\Http\Requests\admin\BlogRequest;
use App\Http\Requests\api\CommentRequest;
use App\Http\Requests\api\RateBlogRequest;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\CommentBlog;
use App\Models\Rating;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class APIBlogController extends Controller
{   
    public $successStatus = 200;
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function list()
    {
        
        $getBlogListComment = Blog::with('comment')->paginate(config('admin.paginate'));
        return response()->json([
            'blog' => $getBlogListComment
        ]);
        
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if(!empty($id)) {

            // $getBlogDetail = Blog::with('comment')->find($id)->orderBy('comment.id', 'desc');

            $getBlogDetail = Blog::with(['comment' => function ($q) {
              $q->orderBy('id', 'desc');
            }])->find($id);

            return response()->json([
                'status' => 200,
                'data' => $getBlogDetail
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getBlog = Blog::find($id)->toArray();
        return response()->json([
            'success' => 'success',
            'data' => $getBlog
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddBlogRequest $request, $id)
    {   

        $blog = Blog::findOrFail($id);
       
        $deleteImage = 'upload/blog/'.$blog['image'];
        $data = $request->all();
        $data['id_auth'] = Auth::id();
        $file = $request->image;
        if(!empty($file)){
            $duoiImage = $file->getClientOriginalExtension();
            $data['image'] = $blog['title'].'.'.$duoiImage;
            $path = public_path('upload/blog/' . $data['image']);
        }
        
        if ($blog->update($data)) {
            if(!empty($file)){
                if(!empty($deleteImage))
                {   
                    unlink($deleteImage);
                }
               Image::make($file->getRealPath())->resize(846, 387)->save($path);

            }
            return response()->json([
                'success' => 'success',
                'data' => $data
            ]);
        } else {
            return response()->json([
                'error' => 'error',
            ]);
        }
    }
    // public function delete($id)
    // {
    //     $blog = DB::table('blog')->where('id',$id)->delete();
    //     if ($blog) {
 
    //         return redirect('/admin/blog')->with('success', 'Delete blog success.');
    //     } else {

    //         return redirect('/admin/blog')->withErrors('Delete blog error.');
    //     }
    // }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function comment(CommentRequest $request, $id)
    {

        $data = $request->all();
        if ($id) {
            $getListComment = CommentBlog::create($data);
            if($getListComment){
                return response()->json([
                    'status' => 200,
                    'data' => $getListComment
                ]);
            } else {
                return response()->json([
                    'status' => 404,
                    'error' => 'error'
                ]);
            }
            
        } else {
                return response()->json([
                    'status' => 200,
                    'error' => 'id not found'
                ]);
        }
    }

    public function pagingBlogDetail(Request $request, $id) {
      
        $blog = Blog::find($id); 
        $previous = Blog::where('id', '<', $blog->id)->max('id');
        $next = Blog::where('id', '>', $blog->id)->min('id');

        if($blog){
            return response()->json([
                'status' => 200,
                'data' => $blog,
                'previous' => $previous,
                'next' => $next
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'error' => 'error'
            ]);
        }

    }
    // get all rate with id
    public function rateBlog($id)
    {
        $getAllRate = Rating::all()->where('blog_id', $id)->toArray();
        return response()->json([
            'response' => 'success',
            'data' => $getAllRate
        ], $this->successStatus);


    }
    // post rate with id
    public function rate(RateBlogRequest $request)
    {
        $input = $request->all();
        if (!empty($input['user_id'])) {
            if (Rating::create($input)) {
                return response()->json([
                  'status' => 200,
                  'message' => 'You have rate this blog successfully.'
                ]);
            } else {
                return response()->json([
                  'status' => 200,
                  'message' => 'Lỗi server , bạn đánh giá k thành công.'
                ]);
            }
        }
    }

}
