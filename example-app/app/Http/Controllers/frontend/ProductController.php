<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\frontend\ProductRequest;
use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::id();
        $myproduct = Product::where('iduser',$user)->get();    
        
        return view('frontend.product.myproduct',compact('myproduct'));
    }
    public function viewadd()
    {
        $category = DB::table('category')->get();
        $brand = DB::table('brand')->get();
        return view('frontend.product.addproduct',compact('category','brand'));
    }
    public function viewedit($id)
    {
        $prod = Product::where('id',$id)->first();
        $category = DB::table('category')->get();
        $brand = DB::table('brand')->get();
        return view('frontend.product.editproduct',compact('category','brand','prod'));
    }

    public function viewhome()
    {
        //$user = Auth::id();
        $category = DB::table('category')->get();
        $brand = DB::table('brand')->get();
        $prod = Product::orderBy('created_at', 'desc')->limit(6)->get();
        return view('frontend.page.index',compact('prod','category','brand'));
    }
    // public function addcart(Request $request){
    //     $id = $request['id'];
        
    //    $prod = Product::where("id",$id)->first();     
        
    //     if(session()->has('cart.'.$prod['id'])){
    //        $val = session()->get('cart.'.$prod['id'].'.qty');
    //        $val++;
    //        $prod->qty = $val;
    //        session()->put('cart.'.$prod['id'],$prod);
    //     //    $val2 = session()->get('cart.'.$prod['id'].'.qty');
    //     //     echo $val2;
    //     }else{
    //         $prod->qty = 1;
    //         session()->put('cart.'.$prod['id'],$prod);
    //     }
    //     $sum = 0;
    //     foreach(session('cart') as $item){
    //         $sum += $item['qty'];
    //     }
    //     echo $sum;
    //     //echo session('cart.'.$prod['id'].'.qty');
    //     //session()->forget('cart');
    // }
    // public function showcart(){
    //     return view('frontend.cart.showcart');
    // } 
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductRequest $request)
    {
        $data = $request->all();
        //dd($request->all());
        $data['iduser'] = Auth::id();
        if($request->hasFile('image')){
            foreach ($request->file('image') as $item) {
                $name = $item->getClientOriginalName();
                $name2 = "anh_85".$item->getClientOriginalName();
                $name3 = "anh_329".$item->getClientOriginalName();

                $path = public_path('upload/product/' . $name);
                $path2 = public_path('upload/product/' . $name2);
                $path3 = public_path('upload/product/' . $name3);

                Image::make($item->getRealPath())->save($path);
                Image::make($item->getRealPath())->resize(85, 84)->save($path2);
                Image::make($item->getRealPath())->resize(329, 380)->save($path3);

                $image[] = $name;
            }
        }
        $data['image'] = json_encode($image);
        if(Product::create($data)){
            
            return redirect('account/myproduct')->with('success',('Thêm thành công'));

        }else{
            return back()->withErrors( 'Your images has been successfully');
        }
    }

    /**
     * Display the specified resource.
     */
    public function detail($id)
    {
        $prod = Product::where('id',$id)->first();
        $brand = DB::table('brand')->where('id',$prod->id_brand)->first();
        return view('frontend.product.detailproduct',compact('prod','brand'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ProductRequest $request,string $id)
    {
        $prod = Product::where('id',$id)->first();
        $data = $request->all();
        
        if(!empty($request->deimage)){
            $delimage = $request->deimage;
            $imageold = json_decode($prod->image, true);                
            $conimage = array_diff($imageold,$delimage);
            $conimage = array_values($conimage);
        }else{
            $conimage = json_decode($prod->image, true);
        }           
        
        if($request->hasFile('image')){          
            if(count($conimage) + count($request->file('image')) > 3){
                return back()->withErrors( 'Chỉ được tối đa 3 ảnh');
            }else{
                //$newimage = array_merge($conimage,$request->file('image'));
                //dd($newimage);
                foreach ($request->file('image') as $item) {
                    
                    $name = $item->getClientOriginalName();
                    $name2 = "anh_85".$item->getClientOriginalName();
                    $name3 = "anh_329".$item->getClientOriginalName();
    
                    $path = public_path('upload/product/' . $name);
                    $path2 = public_path('upload/product/' . $name2);
                    $path3 = public_path('upload/product/' . $name3);
    
                    Image::make($item->getRealPath())->save($path);
                    Image::make($item->getRealPath())->resize(85, 84)->save($path2);
                    Image::make($item->getRealPath())->resize(329, 380)->save($path3);
    
                    $conimage[] = $name;
                }
                $data['image'] = json_encode($conimage);
                if($prod->update($data)){
                    return redirect('account/myproduct')->with('success',('Sửa thành công'));
                }else{
                    return back()->withErrors( 'Your images has been successfully');
                }
            }
        }else{
            //$data['image'] = json_encode($conimage);
                if($prod->update($data)){
                    return redirect('account/myproduct')->with('success',('Sửa thành công'));
                }else{
                    return back()->withErrors( 'Your images has been successfully');
                }
        }

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if(Product::destroy($id)){
            return redirect()->back()->with('success','Đã xoá');
        }else{
            return redirect()->back()->withErrors('Lỗi');
            
        }
    }
}
