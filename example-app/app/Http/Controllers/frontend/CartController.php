<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Product;
use App\Models\User;
use Illuminate\Contracts\Session\Session as SessionSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Exception;
use App\Mail\MailNotify;
use App\Models\History;
use Illuminate\Support\Facades\Mail ;
use Illuminate\Support\Facades\DB;

use function Laravel\Prompts\table;

class CartController extends Controller
{
    public function addcart(Request $request){
        $id = $request['id'];
        
       $prod = Product::where("id",$id)->first();     
        
        if(session()->has('cart.'.$id)){
           $val = session()->get('cart.'.$id.'.qty');
           $val++;
           $prod->qty = $val;
           session()->put('cart.'.$id,$prod);
        //    $val2 = session()->get('cart.'.$prod['id'].'.qty');
        //     echo $val2;
        }else{
            $prod->qty = 1;
            session()->put('cart.'.$id,$prod);
        }
        $sum = 0;
        foreach(session('cart') as $item){
            $sum += $item['qty'];
        }
        echo $sum;
        //echo session('cart.'.$prod['id'].'.qty');
        //session()->forget('cart');
    }
    public function showcart(){
        return view('frontend.cart.showcart');
    } 

    public function update(Request $request){
        $id = $request['getId'];
       $prod = Product::where("id",$id)->first();     
        if(isset($request['action'])){
            if($request['action'] == 'tang'){
                $val = session()->get('cart.'.$id.'.qty');
                $val++;
                $prod->qty = $val;
                session()->put('cart.'.$id,$prod);
            }else if($request['action'] == 'giam') {
                $val = session()->get('cart.'.$id.'.qty');
                $val--;
                if($val == 0){
                    session()->forget('cart.'.$id);
                }else{
                    $prod->qty = $val;
                    session()->put('cart.'.$id,$prod);
                }    
            }else{
                session()->forget('cart.'.$id);
            }
        }
       // print_r(session('cart'));
    } 

    public function search(Request $req){
        $name = $req['input'];
        $prod = Product::where('name','like','%'.$name.'%')->get();
        return view('frontend.cart.search',compact('prod'));
    }

    public function search_price(Request $req){
        $min = $req['minValue'];
        $max = $req['maxValue'];
        $prod = Product::where('price','<',$max)->where('price','>',$min)->get();
        return view('frontend.cart.searchprice',compact('prod'));
    }

    public function checkout(){
        $country = Country::all();
        return view('frontend.cart.checkout',compact('country'));
    }
    public function sendcheck(){
        $id = Auth::id();
        $user = User::where('id',$id)->first();
        $data = session('cart');
        $sumprice = 0;
        // dd($user);
        foreach(session('cart') as $item){
            $sumprice += $item['qty']*$item['price'];
        }
        try{
            Mail::to($user['email'])->send(new MailNotify($data));
            $his = [
                'email' => $user['email'],
                'phone' => $user['phone'],
                'name' => $user['name'],
                'id_user'=>$id,
                'price'=>$sumprice
            ];
            History::create($his);
            return redirect('index')->with('success',('Kiểm tra email'));

        }catch(Exception $err){
            var_dump($err);
            return redirect('index')->withErrors('Gửi mail thất bại');
        }
    }


    public function viewsearchadvanced(){
        $category = DB::table('category')->get();
        $brand = DB::table('brand')->get();
        $prod = Product::paginate(6);
        return view('frontend.cart.searchadvanced',compact('category','brand','prod'));
    }

    public function searchadvanced(Request $request){
        $req = [
            'name' => $request->name,
            'price' => $request->price,
            'id_category' => $request->id_category,
            'id_brand' => $request->id_brand,
            'status' => $request->status,
        ];
        //dd($req);
        $prod = Product::query();
        $check = false;
        foreach($req as $key => $value){
            if($key == 'price'&&!empty($value)){
                $arr = json_decode($value, true);
                $prod->where($key,'>=',$arr[0])->where($key,'<=',$arr[1]);
                $check = true;
            }else{
                if(!empty($value) || $key == 'status'&&$value=='0'){
                    $prod->where($key,$value)->get();
                    $check = true;
                }
            }          
        }
        //dd($check);
        if($check){
            $category = DB::table('category')->get();
            $brand = DB::table('brand')->get();
            $prod = $prod->get();
            return view('frontend.cart.searchadvanced',compact('category','brand','prod','check'));
        }else{
            $category = DB::table('category')->get();
            $brand = DB::table('brand')->get();
            $prod = Product::paginate(6);
            return view('frontend.cart.searchadvanced',compact('category','brand','prod','check'));
        }
        
    }
}
