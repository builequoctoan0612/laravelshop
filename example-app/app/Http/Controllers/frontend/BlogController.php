<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Rating;
use App\Models\User;
use App\Models\CommentBlog;
use Illuminate\Support\Facades\Auth;



class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $blog = Blog::paginate(3);
        return view('frontend.blog.blog',compact('blog'));
    }
    public function indexdetail($id){
        $blogd = Blog::findOrFail($id);
        $pre = Blog::where('id', '<', $id)->orderBy('id', 'desc')->first();
        $next = Blog::where('id', '>', $id)->orderBy('id', 'asc')->first();

        $avgrate = Rating::where('blog_id',$blogd->id)->avg('rate');
        $countrate = Rating::where('blog_id',$blogd->id)->count();
        $avgrate = round($avgrate);

        $cmt = CommentBlog::where('blog_id',$id)->get();
        
        return view('frontend.blog.blogdetail',compact('blogd','pre', 'next','avgrate','countrate','cmt'));
    }
    public function rating(Request $request){
      
        $userId = Auth::id();
        $rate = new Rating();
        $rate->user_id = $userId;
        $rate->blog_id = $request['blog_id'];
        $rate->rate = $request['rate'];
        $rate->save();
        echo 'ok';
    }

    public function comment(Request $request){
         $user = Auth::user();       
        $cmt = new CommentBlog();
        $cmt->user_id = $user->id;
        $cmt->avatar = $user->avatar;
        $cmt->name = $user->name;
        $cmt->blog_id = $request['blogid'];
        $cmt->content = $request['content'];
        $cmt->level = $request['level'];
        $cmt->save();        
        $blog = CommentBlog::where('blog_id',$request['blogid'])->get();
        
        return response()->json(['data' => $blog]);
    }

    public function replycomment(Request $request){
        $user = Auth::user();       
       $cmt = new CommentBlog();
       $cmt->user_id = $user->id;
       $cmt->avatar = $user->avatar;
       $cmt->name = $user->name;
       $cmt->blog_id = $request['blogid'];
       $cmt->content = $request['content'];
       $cmt->level = $request['level'];
       $cmt->save();
       $blog = CommentBlog::where('blog_id',$request['blogid'])->where('level',$request['level'])->get();
        
        return response()->json(['data' => $blog]);
   }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
