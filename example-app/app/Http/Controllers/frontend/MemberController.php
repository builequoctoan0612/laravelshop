<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\UpdateProfileRequest;
use App\Http\Requests\frontend\LoginRequest;
use App\Http\Requests\frontend\RegisterRequest;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $country = Country::all();
        
        return view('frontend.member.register',compact('country'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(RegisterRequest $request)
    {
        $data = $request->all();
        $data['level']=0;
        $file = $request->avatar;
        
        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }

        if($request->has('data')){
            User::create($data);
            $login = [
                'email'=>$data['email'],
                'password'=>$data['password'],
                'level'=>0
            ];
            $remember = false;
            if(Auth::attempt($login,$remember)){
                return redirect('checkout');
            }else{
                return redirect()->back()->withErrors('Sai tài khoản hoặc mật khẩu');
            }               
        }else{
            if(User::create($data)){
                if(!empty($file)){
                    $file->move('upload/user/avatar',$file->getClientOriginalName());
                }
                return redirect('loginmb')->with('success',('Đăng kí thành công'));          
            }else{
                return redirect()->back()->withErrors('Thất bại');

            }  
        }
        
    }

    public function indexlog(){
        return view('frontend.member.login');
    }
    public function login(LoginRequest $request){
        $login = [
            'email'=>$request->email,
            'password'=>$request->password,
            'level'=>0
        ];
        $remember = false;
        if($request->remember_me){
            $remember = true;
        }
        if(Auth::attempt($login,$remember)){
            return redirect('index');
        }else{
            return redirect()->back()->withErrors('Sai tài khoản hoặc mật khẩu');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect("/loginmb");
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function viewupdate(){
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $country = Country::all();
        return view('frontend.member.profile',compact('user','country'));
    }
    public function update(UpdateProfileRequest $request)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
