<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddBlogRequest;

use Illuminate\Http\Request;
use App\Models\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $blog = Blog::paginate(3);       
        return view('Admin.blog.blog',compact('blog'));
    }
    public function viewcreate(){
        return view('Admin.blog.addblog');
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create(AddBlogRequest $request)
    {
        
        $data = $request->all();
        
        $data['image'] = $request->image->getClientOriginalName();
        if(Blog::create($data)){
            $file = $request->image;
            $file->move('upload/blog',$file->getClientOriginalName());
            return redirect('blog')->with('success',('Update profile success'));
        }else{
            return redirect()->back()->withErrors('Update profile success');

        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
       
        $data = Blog::where('id',$id)->first();
        //dd($data);
        return view('Admin.blog.editblog',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $blog = Blog::findOrFail($id);
        $data = $request->all();
        $data['image'] = $request->image->getClientOriginalName();
        if($blog->update($data)){
            return redirect('blog')->with('success','Thành Công');
        }else{
            return redirect('blog')->withErrors('Thất Bại');

        }      
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if(Blog::destroy($id)){
            return redirect()->back()->with('success','Đã xoá');
        }else{
            return redirect()->back()->withErrors('Lỗi');
            
        }
    }
}
