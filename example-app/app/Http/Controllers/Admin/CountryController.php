<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\admin\AddCountryRequest;
class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $country = Country::all();
        return view('Admin.country.country',compact('country'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function viewcreate(){
        return view('Admin.country.addcountry');
    }
    public function create(AddCountryRequest $request)
    {
        $data = $request->all();
        if(Country::create($data)){
            //return redirect()->back()->with('success',('Update profile success'));
            return redirect('country')->with('success',('Update profile success'));
        }else{
            return redirect()->back()->withErrors('Update profile success');
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {        
        if(Country::destroy($id)){
            return redirect()->back()->with('success','Đã xoá');
        }else{
            return redirect()->back()->withErrors('Lỗi');
            
        }
    }
}
