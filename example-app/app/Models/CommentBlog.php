<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentBlog extends Model
{
    use HasFactory;
    protected $table = 'cmtblog';
    protected $fillable = [
        'content','blog_id','user_id','avatar','name','level'
    ];
    public $timestamps = true;

}
