@extends('Admin.layouts.app')
@section('content')

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Country</h4>
            <h6 class="card-subtitle">Similar to tables, use the modifier classes .thead-light to make <code>&lt;thead&gt;</code>s appear light.</h6>
        </div>
        @if (session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
            {{session('success')}}
        </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
        <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
        <ul>
            @foreach ($errors->all() as $er)
                <li>{{$er}}</li>
            @endforeach
            
        </ul>
    </div>
@endif
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>  
                        <th scope="col">Delete</th>  
                                         
                    </tr>
                </thead>
                <tbody>
                    @foreach ($country as $item)
                        <tr>
                            <th scope="row">{{$item->id}}</th>
                            <td>{{$item->name}}</td>
                            <td><a href="{{url('/country/'.$item->id)}}"> Delete</a></td>
                        </tr> 
                       
                    @endforeach
                                       
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <a href="{{url('addcountry')}}"><button class="btn btn-success">ADD</button></a>
            </div>
        </div>
    </div>
</div>
@endsection