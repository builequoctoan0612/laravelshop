@extends('Admin.layouts.app')
@section('content')
@if (session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
            {{session('success')}}
        </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
        <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
        <ul>
            @foreach ($errors->all() as $er)
                <li>{{$er}}</li>
            @endforeach
            
        </ul>
    </div>
@endif
<form class="form-horizontal form-material" method="POST" >
    @csrf
<div class="form-group">
    <label class="col-md-12">Name</label>
    <div class="col-md-12">
        <input type="text" placeholder="" class="form-control form-control-line" name="name">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        <button class="btn btn-success" type="submit">Add</button>
    </div>
</div>
</form>
@endsection