@extends('Admin.layouts.app')
@section('content')
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
        <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
        <ul>
            @foreach ($errors->all() as $er)
                <li>{{$er}}</li>
            @endforeach
            
        </ul>
    </div>
@endif
<form class="form-horizontal form-material" enctype="multipart/form-data" method="POST" >
    @csrf
<div class="form-group">
    <label class="col-md-12">Title</label>
    <div class="col-md-12">
        <input type="text" placeholder="" class="form-control form-control-line" name="title">
    </div>
</div>
<div class="form-group">
    <label class="col-md-12">Image</label>
    <div class="col-md-12">
        <input type="file" placeholder="" class="form-control form-control-line" name="image">
    </div>
</div>
<div class="form-group">
    <label class="col-md-12">Description</label>
    <div class="col-md-12">
        <input type="text" placeholder="" class="form-control form-control-line" name="description">
    </div>
</div>
<div class="form-group">
    <label class="col-md-12">Content</label>
    <div class="col-md-12">
        <textarea type="text"  id="demo" class="form-control form-control-line" name="content"></textarea>  
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        <button class="btn btn-success" type="submit">Add</button>
    </div>
</div>
</form>
@endsection