@extends('Admin.layouts.app')
@section('content')

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Country</h4>
            <h6 class="card-subtitle">Similar to tables, use the modifier classes .thead-light to make <code>&lt;thead&gt;</code>s appear light.</h6>
        </div>
        @if (session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
            {{session('success')}}
        </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
        <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
        <ul>
            @foreach ($errors->all() as $er)
                <li>{{$er}}</li>
            @endforeach
            
        </ul>
    </div>
@endif
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>  
                        <th scope="col">Image</th>  
                        <th scope="col">Description</th>  
                        <th scope="col">Content</th>                                          
                        <th scope="col">Action</th>                                          
                    </tr>
                </thead>
                <tbody>
                    @foreach ($blog as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->title}}</td>
                            <td>{{$item->image}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->content}}</td>
                            <td><a href="{{url('editblog/'.$item->id)}}">Edit</a>
                                <a href="{{url('/blog/'.$item->id)}}"> Delete</a>                               
                            </td>
                        </tr> 
                       
                    @endforeach
                                       
                </tbody>

            </table>
           <div style="float: right">{{$blog->links()}} </div>                
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <a href="{{url('addblog')}}"><button class="btn btn-success">ADD</button></a>
            </div>
        </div>
        
    </div>
</div>
@endsection