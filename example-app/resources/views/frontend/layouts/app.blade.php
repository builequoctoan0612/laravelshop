<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('frontend/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('frontend/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="{{asset('frontend/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('frontend/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('frontend/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('frontend/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('frontend/images/ico/apple-touch-icon-57-precomposed.png')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('frontend/css/rate.css')}}">
    <script src="{{asset('frontend/js/jquery-1.9.1.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head><!--/head-->

<body>
	@include('frontend.layouts.header')
	
	@include('frontend.layouts.slide')

    @yield('cart')
	
    @if (!((request()->is('show-cart')) || request()->is('checkout'))))
        <section>
		<div class="container">
			<div class="row">
                
                @if (request()->is('account*'))
                    @include('frontend.layouts.leftaccount')
                 @else
                    @include('frontend.layouts.left')
                 @endif 
				
				
				<div class="col-sm-9 padding-right">
					@yield('content')
					
				</div>
			</div>
		</div>
	</section>
    @endif
	
	
	@include('frontend.layouts.footer')
	

    <script>
        $(document).ready(function() {
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
            $(".add-to-cart").click(function (e) { 
                e.preventDefault();
                var id = $(this).data("id");
                //alert(id)
                $.ajax({
                    method:"POST",
                    url:"{{'/add-cart'}}",
                    data:{
                        id:id
                    },
                    success : function(res){
                       // $('.sum-cart').append(res);
                        $('.sum-cart').text(res);
                    }
                });
            });
            var slider = $('#sl2').slider(); // Khởi tạo slider

            // Bắt sự kiện khi giá trị slider thay đổi
            slider.on('slide', function(event) {
                var values = event.value;
                var minValue = values[0];
                var maxValue = values[1];

                $.ajax({
                    method:"POST",
                    url:"{{'/search-price'}}",
                    data:{
                        minValue:minValue,
                        maxValue:maxValue
                    },
                    success : function(res){

                        $('.features_items').html(res)
                    }
                });
            });
        });
    </script>

    <script src="{{asset('frontend/js/jquery.js')}}"></script>
	<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('frontend/js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('frontend/js/price-range.js')}}"></script>
    <script src="{{asset('frontend/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('frontend/js/main.js')}}"></script>
</body>
</html>