@extends('frontend.layouts.app')
@section('content')
<div class="col-sm-9">
    <div class="blog-post-area">
        <h2 class="title text-center">Latest From our Blog</h2>
        <div class="single-blog-post">
            <h3>{{$blogd->title}}</h3>
            <div class="post-meta">
                <ul>
                    <li><i class="fa fa-user"></i> Mac Doe</li>
                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                </ul>
                <!-- <span>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                </span> -->
            </div>
            <a href="">
                <img src="{{asset('upload/blog/'.$blogd->image)}}" alt="">
            </a>
            <p>{{$blogd->content}}  </p>
            <div class="pager-area">
                <ul class="pager pull-right">
                    @if ($pre)
                        <a href="{{ url('/blogdetail/'.$pre->id) }}">Previous</a>
                    @endif
                    @if ($next)
                        <a href="{{ url('/blogdetail/'.$next->id) }}">Next</a>
                    @endif
                </ul>
            </div>
        </div>
    </div><!--/blog-post-area-->

    <div class="rating-area">
        {{-- <ul class="ratings">
            <li class="rate-this">Rate this item:</li>
            <li>
                <i class="fa fa-star color"></i>
                <i class="fa fa-star color"></i>
                <i class="fa fa-star color"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </li>
            <li class="color">(6 votes)</li>
        </ul> --}}
        
        <div class="rate">
            <div class="vote" data-blogid="{{$blogd->id}}">
                @for ($i = 1; $i < 6; $i++)
                    @if ($i <= $avgrate)
                        <div class="star_1 ratings_stars ratings_over" data-index="1"><input value="{{$i}}" type="hidden"></div>
                    @else
                        <div class="star_1 ratings_stars" data-index="1"><input value="{{$i}}" type="hidden"></div>  
                    @endif                   
                                                   
                @endfor
                {{-- <div class="star_1 ratings_stars" data-index="1"><input value="1" type="hidden"></div>
                <div class="star_2 ratings_stars" data-index="2"><input value="2" type="hidden"></div>
                <div class="star_3 ratings_stars" data-index="3"><input value="3" type="hidden"></div>
                <div class="star_4 ratings_stars" data-index="4"><input value="4" type="hidden"></div>
                <div class="star_5 ratings_stars" data-index="5"><input value="5" type="hidden"></div> --}}
                <span class="rate-np">{{$countrate}} votes</span>
            </div> 
        </div>
        <ul class="tag">
            <li>TAG:</li>
            <li><a class="color" href="">Pink <span>/</span></a></li>
            <li><a class="color" href="">T-Shirt <span>/</span></a></li>
            <li><a class="color" href="">Girls</a></li>
        </ul>
    </div><!--/rating-area-->

    <div class="socials-share">
        <a href=""><img src="{{asset('frontend/images/blog/socials.png')}}" alt=""></a>
    </div><!--/socials-share-->

    <!-- <div class="media commnets">
        <a class="pull-left" href="#">
            <img class="media-object" src="images/blog/man-one.jpg" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">Annie Davis</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            <div class="blog-socials">
                <ul>
                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                </ul>
                <a class="btn btn-primary" href="">Other Posts</a>
            </div>
        </div>
    </div> --><!--Comments-->
    <div class="response-area">
        <h2>3 RESPONSES</h2>
        <ul class="media-list">
            @foreach ($cmt as $item)
                @if ($item->level == 0)
                    <li class="media media-{{$item->id}}">
                        <a class="pull-left" href="#">
                            <img class="media-object" src="{{asset('upload/user/avatar/'.$item->avatar)}}" style="width: 50px" alt="">
                        </a>
                        <div class="media-body">
                            <ul class="sinlge-post-meta">
                                <li><i class="fa fa-user"></i>{{$item->name}}</li>
                                <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <p>{{$item->content}}</p>
                            <button class="btn btn-primary replycmt" href=""  data-cmtid="{{$item->id}}"><i class="fa fa-reply"></i>Replay</button>                           
                        </div>   
                        <form action="" style="display:none" class="form-rep reply-{{$item->id}}">
                            <div class="form-group">                               
                                <textarea name="replycmtt" id="content-rep-{{$item->id}}" ></textarea>
                            </div>
                            <button type="button" class="btn btn-primary post-replycmt" data-blogid="{{$blogd->id}}" data-cmtid="{{$item->id}}">Reply</button>
                        </form>                                        
                @endif                       
                @foreach ($cmt as $con)
                    @if ($item->id == $con->level)
                        <li class="media second-media">
                        <a class="pull-left" href="#">
                            <img class="media-object" src="{{asset('upload/user/avatar/'.$item->avatar)}}" style="width: 50px" alt="">
                        </a>
                        <div class="media-body">
                            <ul class="sinlge-post-meta">
                                <li><i class="fa fa-user"></i>{{$con->name}}</li>
                                <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <p>{{$con->content}}</p>
                            {{-- <button class="btn btn-primary replycmt" href="" data-blogid="{{$blogd->id}}" data-cmtid="{{$con->id}}"><i class="fa fa-reply"></i>Replay</button> --}}
                        </div>
                        </li>              
                    @endif
                @endforeach
                    </li>      
            @endforeach
            {{-- <form action="" style="display: none;" id="new">
                <li class="media" >
                    <a class="pull-left" href="#">
                        <img class="media-object" src="{{$item->avatar}}" alt="">
                    </a>
                    <div class="media-body">
                        <ul class="sinlge-post-meta" >
                            <li><i class="fa fa-user"></i>{{$item->name}}</li>
                            <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                            <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                        </ul>
                        <p>{{$item->content}}</p>
                        <a class="btn btn-primary replycmt" href="" data-blogid="{{$blogd->id}}" data-cmtid="{{$item->id}}"><i class="fa fa-reply"></i>Replay</a>                           
                    </div>
                </li>
            </form> --}}
            {{-- <li class="media second-media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="images/blog/man-three.jpg" alt="">
                </a>
                <div class="media-body">
                    <ul class="sinlge-post-meta">
                        <li><i class="fa fa-user"></i>Janis Gallagher</li>
                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                </div>
            </li>
            <li class="media second-media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="images/blog/man-three.jpg" alt="">
                </a>
                <div class="media-body">
                    <ul class="sinlge-post-meta">
                        <li><i class="fa fa-user"></i>Janis Gallagher</li>
                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                </div>
            </li>
            <li class="media second-media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="images/blog/man-three.jpg" alt="">
                </a>
                <div class="media-body">
                    <ul class="sinlge-post-meta">
                        <li><i class="fa fa-user"></i>Janis Gallagher</li>
                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                </div>
            </li>
            <li class="media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="images/blog/man-four.jpg" alt="">
                </a>
                <div class="media-body">
                    <ul class="sinlge-post-meta">
                        <li><i class="fa fa-user"></i>Janis Gallagher</li>
                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                </div>
            </li>
            <li class="media second-media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="images/blog/man-three.jpg" alt="">
                </a>
                <div class="media-body">
                    <ul class="sinlge-post-meta">
                        <li><i class="fa fa-user"></i>Janis Gallagher</li>
                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                </div>
            </li>
            <li class="media second-media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="images/blog/man-three.jpg" alt="">
                </a>
                <div class="media-body">
                    <ul class="sinlge-post-meta">
                        <li><i class="fa fa-user"></i>Janis Gallagher</li>
                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                </div>
            </li>
            <li class="media second-media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="images/blog/man-three.jpg" alt="">
                </a>
                <div class="media-body">
                    <ul class="sinlge-post-meta">
                        <li><i class="fa fa-user"></i>Janis Gallagher</li>
                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                </div>
            </li> --}}
        </ul>					
    </div><!--/Response-area-->
    <div class="replay-box">
        <div class="row">
            <div class="col-sm-12">
                <h2>Leave a replay</h2>
                
                <div class="text-area">
                    <div class="blank-arrow">
                        <label>Your Name</label>
                    </div>
                    <span>*</span>                   
                        <textarea name="message" rows="11"></textarea>
                        <button class="btn btn-primary sendcmt" data-blogid="{{$blogd->id}}" href="">post comment</button>
                   
                </div>
            </div>
        </div>
    </div><!--/Repaly Box-->
</div>	
<script>
    	
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        //vote       
        $('.ratings_stars').hover(
            // Handles the mouseover
            function() {
                $(this).prevAll().andSelf().addClass('ratings_hover');
                // $(this).nextAll().removeClass('ratings_vote'); 
            },
            function() {
                $(this).prevAll().andSelf().removeClass('ratings_hover');
                // set_votes($(this).parent());
            }
        );

        $('.ratings_stars').click(function(){
            
            var checklogin = "{{Auth::Check()}}";
            
            if(checklogin){
                //var action = 'rating'

                var Values =  $(this).find("input").val();
                var blog_id = $(this).closest(".vote").data('blogid');
                if ($(this).hasClass('ratings_over')) {
                    $('.ratings_stars').removeClass('ratings_over');
                    $(this).prevAll().andSelf().addClass('ratings_over');
                } else {
                    $(this).prevAll().andSelf().addClass('ratings_over');
                }
                
                $.ajax({
                    method:"POST",
                    url:"{{'/rating/'}}",
                    data:{
                        rate: Values,
                        blog_id:blog_id,
                        //action:action
                        
                    },
                    success : function(res){
                        if(res=='ok'){
                            alert("Bạn đã đánh giá "+Values+" trên 5 sao")
                        }else{
                            alert("Lỗi đánh giá")
                        }
                    }
                });
            }else{
                alert("Bạn phải đăng nhập");
            }
        });

        $('.sendcmt').click(function(){
            var checklogin = "{{Auth::Check()}}";
            //alert('a')
            if(checklogin){
                //var action = 'send'
                var content = $(this).closest('.text-area').find('textarea').val();
                var blogid = $(this).data('blogid');
                var level = 0;
                //alert("a")
                $.ajax({
                    method:"POST",
                    url:"{{'/comment/'}}",
                    data:{
                        content: content,
                        blogid:blogid,
                        level:level,
                        //action:action
                    },
                    success : function(res){
                        //var index = res.data.length -1
                        //console.log(res.data[res.data.length -1]);
                        var temp = res.data[res.data.length -1]
                        //console.log(temp.avatar);
                        //var avatarPath = "{{ asset('upload/user/avatar/' ) }}"+ temp.avatar;
                        var xx = "{{ asset('upload/user/avatar/')}}"+'/'+ temp.avatar;
                        var html1 = '<li class="media">'+
                                '<a class="pull-left" href="#">'+
                                    '<img class="media-object" src="'+xx+'" style="width: 50px" alt="">'+
                                '</a>'+
                                '<div class="media-body">'+
                                    '<ul class="sinlge-post-meta">'+
                                        '<li><i class="fa fa-user"></i>'+temp.name+'</li>'+
                                        '<li><i class="fa fa-clock-o"></i> 1:33 pm</li>'+
                                        '<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>'+
                                    '</ul>'+
                                    '<p>'+temp.content+'</p>'+                      
                                '</div>'+
                            '</li>'						
                            
                        $('.media-list').append(html1);
                        $('textarea').val("");
                    }
                });	
                                
            }else{
                alert('bạn phải đăng nhập')
            }
            
        });
        //$('.replycmt').click(function(e){

        $(document).on('click','.replycmt', function (e) {
            var id = $(this).data('cmtid');           
            $('.form-rep').slideUp();
            $(".reply-"+id).slideDown();
        });
        $(document).on('click','.post-replycmt', function (e) {
            e.preventDefault();
             var checklogin = "{{Auth::Check()}}";           
            if(checklogin){
                var id = $(this).data('cmtid');
                var content = $('#content-rep-'+id).val();               
                var blogid = $(this).data('blogid');
                var level = id;
                $.ajax({
                	method:"POST",
                	url:"{{'/reply'}}",
                	data:{
                		content: content,
                		blogid:blogid,
                		level:level,
                	},
                	success : function(res){
                		var temp = res.data[res.data.length -1]
                        console.log(temp);
                        //var avatarPath = "{{ asset('upload/user/avatar/' ) }}"+ temp.avatar;
                        var xx = "{{ asset('upload/user/avatar/')}}"+'/'+ temp.avatar;
                        var html1 = '<li class="media second-media ">'+
                                '<a class="pull-left" href="#">'+
                                    '<img class="media-object"  src="{'+xx+'" style="width: 50px" alt="">'+
                                '</a>'+
                                '<div class="media-body">'+
                                    '<ul class="sinlge-post-meta">'+
                                        '<li><i class="fa fa-user"></i>'+temp.name+'</li>'+
                                        '<li><i class="fa fa-clock-o"></i> 1:33 pm</li>'+
                                        '<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>'+
                                    '</ul>'+
                                    '<p>'+temp.content+'</p>'+                     
                                '</div>'+
                            '</li>'						
                            
                        $('.media-'+id).append(html1);
                        $('#content-rep-'+id).val(""); 
                	}
                    
                });					
            }else{
                alert('bạn phải đăng nhập')
            }
            
        });
    });
</script>
@endsection