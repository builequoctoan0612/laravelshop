@extends('frontend.layouts.app') 
@section('content')
<section>

            <div class="col-sm-9">
                <div class="blog-post-area">
                    <h2 class="title text-center">Latest From our Blog</h2>
                    @foreach ($blog as $item)
                    <div class="single-blog-post">
                        <h3>{{$item->title}}</h3>
                        <div class="post-meta">
                            <ul>
                                <li><i class="fa fa-user"></i> Mac Doe</li>
                                <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <span>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                            </span>
                        </div>
                        <a href="">
                            <img src="{{asset('upload/blog/'.$item->image)}}" alt="">
                        </a>
                        <p>{{$item->description}}</p>
                        <a  class="btn btn-primary" href="{{url('blogdetail/'.$item->id)}}">Read More</a>
                    </div>   
                    @endforeach                 
                </div>
                <div style="float: right">{{ $blog->links() }}</div>

            </div>

</section>
@endsection