<h2 class="title text-center">Features Items</h2>
@foreach ($prod as $item)
             @php
                 $image = json_decode($item->image, true);                
            @endphp
            
            <div class="col-sm-4">
            <div class="product-image-wrapper">
                <div class="single-products">
                        <div class="productinfo text-center">
                            <img src="{{asset('upload/product/'.$image[0])}}" alt="" />
                            <h2>${{$item->price}}</h2>
                            <p>{{$item->name}}</p>
                            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                        </div>
                        <div class="product-overlay">
                            <div class="overlay-content">
                                <h2>${{$item->price}}</h2>
                                <p>{{$item->name}}</p>
                                <a href="" class="btn btn-default add-to-cart"  data-id="{{$item->id}}"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                            </div>
                        </div>
                </div>
                <div class="choose">
                    <ul class="nav nav-pills nav-justified">
                        <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                        <li><a href="{{url('/account/detailproduct/'.$item->id)}}"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
        <script>
            $(document).ready(function() {
                $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
                $(".add-to-cart").click(function (e) { 
                    e.preventDefault();
                    var id = $(this).data("id");
                    //alert(id)
                    $.ajax({
                        method:"POST",
                        url:"{{'/add-cart'}}",
                        data:{
                            id:id
                        },
                        success : function(res){
                           // $('.sum-cart').append(res);
                            $('.sum-cart').text(res);
                        }
                    });
                });
            });
        </script>