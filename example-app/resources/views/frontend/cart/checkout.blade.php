@extends('frontend.layouts.app')
@section('cart')
@guest
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
        
            <div class="col-sm-4">
                <div class="signup-form"><!--sign up form-->
                    <h2>New User Signup!</h2>
                    <form action="{{url('/registermb') }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <input type="text" placeholder="Name" name='name'/>
                        <input type="email" placeholder="Email Address" name='email'/>
                        <input type="password" placeholder="Password" name='password'/>
                        <input type="text" placeholder="Phone" name='phone'/>
                        <input type="text" placeholder="Address" name='address'/>
                        <input type="file" placeholder="Avatar"  name="avatar">
                        <input type="hidden" name="data" value='test'>
                        <select name="id_country">
                            @foreach ($country as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-default">Signup</button>
                    </form>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section>
@endguest
@auth
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Shopping Cart</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @php
                        //print_r(session('cart'));
                        $tong = 0;
                    @endphp
                    @if (!session('cart'))
                        <h3>Vui lòng thêm vào hàng</h3>
                    @else
                        @foreach (session('cart') as $index => $item)
                            @php
                                $total = $item['price']*$item['qty'];
                                $tong += $total;
                                $image = json_decode($item->image, true);                                        
                            @endphp
                            <tr>
                                <td class="cart_product" id="{{$item['id']}}">
                                    <a href=""><img src="{{asset('upload/product/'.$image[0])}}" style="width: 90px" alt=""></a>
                                </td>
                                <td class="cart_description">
                                    <h4><a href="">{{$item['name']}}</a></h4>
                                    <p>Web ID: 1089772</p>
                                </td>
                                <td class="cart_price">
                                    <p>${{$item['price']}}</p>
                                </td>
                                <td class="cart_quantity">
                                    <div class="cart_quantity_button">
                                        <a class="cart_quantity_up" href=""> + </a>
                                        <input class="cart_quantity_input" type="text" name="quantity" value="{{$item['qty']}}" autocomplete="off" size="2">
                                        <a class="cart_quantity_down" href=""> - </a>
                                    </div>
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">${{$total}}</p>
                                </td>
                                <td class="cart_delete">
                                    <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                                </td>
                            </tr> 
                        @endforeach
                        <tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>${{$tong}}</td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>$0</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td><span>${{$tong}}</span></td>
									</tr>
								</table>
							</td>
						</tr>
                    @endif    
                </tbody>
            </table>
            <form action="" method="post">
                @csrf
                <button class="btn btn-primary" style="float: right" href="">Continue</button>
            </form>        
        </div>
    </div>
</section> 
@endauth<!--/#cart_items-->
@endsection