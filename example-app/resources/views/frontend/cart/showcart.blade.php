@extends('frontend.layouts.app')
@section('cart')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Shopping Cart</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @php
                        //print_r(session('cart'));
                        $tong = 0;
                    @endphp
                    @if (!session('cart'))
                        <h3>Vui lòng thêm vào hàng</h3>
                    @else
                        @foreach (session('cart') as $index => $item)
                            @php
                                $total = $item['price']*$item['qty'];
                                $tong += $total;
                                $image = json_decode($item->image, true);                                        
                            @endphp
                            <tr>
                                <td class="cart_product" id="{{$item['id']}}">
                                    <a href=""><img src="{{asset('upload/product/'.$image[0])}}" style="width: 90px" alt=""></a>
                                </td>
                                <td class="cart_description">
                                    <h4><a href="">{{$item['name']}}</a></h4>
                                    <p>Web ID: 1089772</p>
                                </td>
                                <td class="cart_price">
                                    <p>${{$item['price']}}</p>
                                </td>
                                <td class="cart_quantity">
                                    <div class="cart_quantity_button">
                                        <a class="cart_quantity_up" href=""> + </a>
                                        <input class="cart_quantity_input" type="text" name="quantity" value="{{$item['qty']}}" autocomplete="off" size="2">
                                        <a class="cart_quantity_down" href=""> - </a>
                                    </div>
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">${{$total}}</p>
                                </td>
                                <td class="cart_delete">
                                    <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                                </td>
                            </tr> 
                        @endforeach
                    @endif
                    
                    

                    {{-- <tr>
                        <td class="cart_product">
                            <a href=""><img src="images/cart/two.png" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">Colorblock Scuba</a></h4>
                            <p>Web ID: 1089772</p>
                        </td>
                        <td class="cart_price">
                            <p>$59</p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <a class="cart_quantity_up" href=""> + </a>
                                <input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
                                <a class="cart_quantity_down" href=""> - </a>
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">$59</p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="cart_product">
                            <a href=""><img src="images/cart/three.png" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">Colorblock Scuba</a></h4>
                            <p>Web ID: 1089772</p>
                        </td>
                        <td class="cart_price">
                            <p>$59</p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <a class="cart_quantity_up" href=""> + </a>
                                <input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
                                <a class="cart_quantity_down" href=""> - </a>
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">$59</p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                        </td>
                    </tr> --}}
                </tbody>
            </table>
        </div>
    </div>
</section> <!--/#cart_items-->

<section id="do_action">
    <div class="container">
        <div class="heading">
            <h3>What would you like to do next?</h3>
            <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="chose_area">
                    <ul class="user_option">
                        <li>
                            <input type="checkbox">
                            <label>Use Coupon Code</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Use Gift Voucher</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Estimate Shipping & Taxes</label>
                        </li>
                    </ul>
                    <ul class="user_info">
                        <li class="single_field">
                            <label>Country:</label>
                            <select>
                                <option>United States</option>
                                <option>Bangladesh</option>
                                <option>UK</option>
                                <option>India</option>
                                <option>Pakistan</option>
                                <option>Ucrane</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                            
                        </li>
                        <li class="single_field">
                            <label>Region / State:</label>
                            <select>
                                <option>Select</option>
                                <option>Dhaka</option>
                                <option>London</option>
                                <option>Dillih</option>
                                <option>Lahore</option>
                                <option>Alaska</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                        
                        </li>
                        <li class="single_field zip-field">
                            <label>Zip Code:</label>
                            <input type="text">
                        </li>
                    </ul>
                    <a class="btn btn-default update" href="">Get Quotes</a>
                    <a class="btn btn-default check_out" href="">Continue</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="total_area">
                    <ul>
                        <li>Cart Sub Total <span>$59</span></li>
                        <li>Eco Tax <span>$2</span></li>
                        <li>Shipping Cost <span>Free</span></li>
                        <li>Total <span id='total'>${{$tong}}</span></li>
                    </ul>
                        <a class="btn btn-default update" href="">Update</a>
                        <a class="btn btn-default check_out" href="">Check Out</a>
                </div>
            </div>
        </div>
    </div>
</section><!--/#do_action-->
<script>
    $(document).ready(function () {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        $(".cart_quantity_up").click(function(){
            var tong = $("#total").text().replace("$","");
            var value = $(this).closest("tr").find(".cart_quantity_input").val();
            var id = $(this).closest("tr").find(".cart_product").attr("id");
            var price = $(this).closest("tr").find(".cart_price").text().replace("$", "")
            var total = $(this).closest("tr").find(".cart_total_price").text().replace("$", "");
            if (value = parseInt(value) + 1) {					
                tong=parseInt(price) + parseInt(tong);
                total = parseInt(total) + parseInt(price)
                $(this).closest("tr").find(".cart_total_price").text("$" + total);
                $(this).closest("tr").find(".cart_quantity_input").val(parseInt(value));
                $("#total").text("$" + tong);

                $.ajax({
                method:"POST",
                url:"{{'/update-qty-cart'}}",
                data:{
                    getId: id,
                    action: 'tang'
                }   
            });
            }				
            event.preventDefault();
        });
        $(".cart_quantity_down").click(function(){
            var tong = $("#total").text().replace("$","");
            var value = $(this).closest("tr").find(".cart_quantity_input").val();
            var id = $(this).closest("tr").find(".cart_product").attr("id");
            var price = $(this).closest("tr").find(".cart_price").text().replace("$", "")
            var total = $(this).closest("tr").find(".cart_total_price").text().replace("$", "");
            if (value > 1) {
                value = parseInt(value) - 1					
                tong=parseInt(tong) - parseInt(price) ;
                total = parseInt(total) - parseInt(price);
                $(this).closest("tr").find(".cart_total_price").text("$" + total);
                $(this).closest("tr").find(".cart_quantity_input").val(parseInt(value));
                $("#total").text("$" + tong);					
            }else{
                tong = parseInt(tong) - parseInt(price);
                $("#total").text("$" + tong);
                $(this).closest("tr").remove()
            }
                $.ajax({
                method:"POST",
                url:"{{'/update-qty-cart'}}",
                data:{
                    getId: id,
                    action: 'giam'
                }					               	
                });			
            event.preventDefault();
        });

        $(".cart_quantity_delete").click(function () { 
            var tong = $("#total").text().replace("$","");
            var total = $(this).closest("tr").find(".cart_total_price").text().replace("$", "");
            tong = parseInt(tong) - parseInt(total);
            $("#total").text("$" + tong);
            var id = $(this).closest("tr").find(".cart_product").attr("id");
            $(this).closest("tr").remove()
            $.ajax({
                method:"POST",
                url:"{{'/update-qty-cart'}}",
                data:{
                    getId:id,
                    action:'xoa'
                }
            });
            event.preventDefault();
            
        });
    });
</script>
@endsection