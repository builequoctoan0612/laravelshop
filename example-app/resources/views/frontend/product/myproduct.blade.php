@extends('frontend.layouts.app')
@section('content')
<div class="col-12">
    <div class="card">
        
        @if (session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
            {{session('success')}}
        </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
        <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
        <ul>
            @foreach ($errors->all() as $er)
                <li>{{$er}}</li>
            @endforeach
            
        </ul>
    </div>
@endif
<div class="table-responsive cart_info">
   @if (count($myproduct) < 1)
            <h1>Không có sản phẩm</h1>
    @else  
    <table class="table table-condensed">
        <thead>
            <tr class="cart_menu">
                <td class="description">ID</td>
                <td class="description">Name</td>
                <td class="image">Image</td>              
                <td class="price">Price</td>
                {{-- <td class="quantity">Quantity</td> --}}
                <td class="total">Action</td>
                <td></td>
            </tr>
        </thead>
        
        <tbody> 
            @foreach ($myproduct as $item)
            @php
                $image = json_decode($item->image, true);                
            @endphp
            {{-- {{$image}} --}}
                <tr>
                    <td class="cart_description">
                        <h4><a href=""></a></h4>
                        <p>{{$item->id}}</p>
                    </td>
                    <td class="cart_description">
                        <h4><a href=""></a></h4>
                        <p>{{$item->name}}</p>
                    </td>
                     <td class="cart_product">
                         <a href=""><img src="{{asset('upload/product/'.$image[0])}}" style="width: 90px" alt=""></a>
                     </td>
                     
                     <td class="cart_price">
                         <p>${{$item->price}}</p>
                     </td>
                     
                     <td class="cart_total">
                        <a class="cart_quantity_edit" href="{{url("account/editproduct/".$item->id)}}">Sửa</a>                      
                     </td>
                     <td class="cart_delete">
                         <a class="cart_quantity_delete" href="{{url("account/myproduct/".$item->id)}}"><i class="fa fa-times"></i></a>                      
                     </td>
                 </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>
@endsection