@extends('frontend.layouts.app')
@section('content')
    
<div class="blog-post-area">
    @if (session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
        <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
        {{session('success')}}
    </div>
@endif
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
    <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
    <ul>
        @foreach ($errors->all() as $er)
            <li>{{$er}}</li>
        @endforeach
        
    </ul>
</div>
@endif
    <h2 class="title text-center">Update user</h2>
     <div class="signup-form"><!--sign up f"orm-->
    <h2>Edit User!</h2>
    <form action="" method="post" enctype="multipart/form-data">
        @csrf
        <input type="text" placeholder="Name" name='name' />

        <input type="text"  placeholder="Price" name="price" />

        <select class="form-control form-control-line" name="id_category">
            <option value="" >Please choose category</option>                                                                                                                                   
            @foreach ($category as $item)                                                    
                <option value="{{$item->id}}" >{{$item->name}}</option>                                                                                                                                   
            @endforeach
        </select>

        <select class="form-control form-control-line" name="id_brand">
            <option value="" >Please choose brand</option>                                                                                                                                   
            @foreach ($brand as $item)                                                    
                <option value="{{$item->id}}" >{{$item->name}}</option>                                                                                                                                   
            @endforeach
        </select>

        <select class="form-control form-control-line" name="status" id='status'>                                                   
                <option value="0" >New</option>                                                                                                                                   
                <option value="1" >Sale</option>                                                                                                                                   
        </select>

        <input type="text" placeholder="%" name='sale' id='sale' style="display: none">

        <input type="text" placeholder="Company Profile" name="company" />

        <input type="file" placeholder="" multiple name='image[]' />

        <textarea name="detail" id="detail" cols="30" rows="10"></textarea>

        <button type="submit" class="btn btn-default">SAVE</button>
    </form>
</div>
</div>
<script>
  $(document).ready(function() {
    $("#status").change(function() {
      if ($(this).val() === "1") {
        $("#sale").show();
      } else {
        $("#sale").hide();
      }
    });
  });
</script>
@endsection