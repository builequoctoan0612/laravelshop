@extends('frontend.layouts.app')
@section('content')
    
<div class="blog-post-area">
    @if (session('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
        <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
        {{session('success')}}
    </div>
@endif
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
    <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
    <ul>
        @foreach ($errors->all() as $er)
            <li>{{$er}}</li>
        @endforeach
        
    </ul>
</div>
@endif
    <h2 class="title text-center">Update user</h2>
     <div class="signup-form"><!--sign up f"orm-->
    <h2>Edit User!</h2>
    <form action="" method="post" enctype="multipart/form-data">
        @csrf
        <input type="text" placeholder="Name" value="{{$prod->name}}" name='name' />

        <input type="text"  placeholder="Price" value="{{$prod->price}}"  name="price" />

        <select class="form-control form-control-line" name="id_category">
            <option value="" >Please choose category</option>                                                                                                                                   
            @foreach ($category as $item) 
                @if ($item->id == $prod->id_category)
                    <option value="{{$item->id}}" selected >{{$item->name}}</option> 
                @else
                <option value="{{$item->id}}" >{{$item->name}}</option> 
                @endif                                                   
                                                                                                                                                  
            @endforeach
        </select>

        <select class="form-control form-control-line" name="id_brand">
            <option value="" >Please choose brand</option>                                                                                                                                   
            @foreach ($brand as $item) 
                @if ($item->id == $prod->id_brand)
                    <option value="{{$item->id}}" selected>{{$item->name}}</option> 
                @else
                <option value="{{$item->id}}" >{{$item->name}}</option>                     
                @endif                                                                                                                                                                                                   
            @endforeach
        </select>

        <select class="form-control form-control-line" name="status" id='status'> 
                @if ($prod->status == 0)
                    <option value="0" selected>New</option>                                                                                                                                   
                    <option value="1" >Sale</option>
                @else
                    <option value="0" >New</option>                                                                                                                                   
                    <option value="1" selected>Sale</option>
                @endif                                                  
                                                                                                                                                   
        </select>

        <input type="text" placeholder="%" value="{{$prod->sale}}" name='sale' id='sale' style="display: none">

        <input type="text" placeholder="Company Profile" value="{{$prod->company}}" name="company" />

        @php
            $image = json_decode($prod->image, true);                
        @endphp
        @foreach ($image as $item)
            <div style="display:flex">
                <img src="{{asset('upload/product/'.$item)}}" alt="" style="width: 75px" srcset="">
                <input type="checkbox" name="deimage[]" id="" value="{{$item}}"> 
            </div>                              
        @endforeach
          
        <input type="file" placeholder="" multiple name='image[]' />

        <textarea name="detail" id="detail" cols="30" rows="10">{{$prod->detail}}</textarea>

        <button type="submit" class="btn btn-default">SAVE</button>
    </form>
</div>
</div>
<script>
  $(document).ready(function() {
    $("#status").change(function() {
      if ($(this).val() === "1") {
        $("#sale").show();
      } else {
        $("#sale").hide();
      }
    });
  });
</script>
@endsection