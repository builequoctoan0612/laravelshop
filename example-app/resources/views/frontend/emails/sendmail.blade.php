<div style="width: 600px;magin: 0 auto">
   <table border="1" cellspacing='0' cellpadding='0' style="width: 100%">
        <thead>
            <tr class="cart_menu">
                <td class="description">Name</td>
                <td class="price">Price</td>
                <td class="quantity">Quantity</td>
                <td class="total">Total</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            @php
                $tong = 0
            @endphp
                @foreach ($data as $index => $item)
                    @php
                        $total = $item['price']*$item['qty'];  
                        $tong += $total;                              
                    @endphp
                    <tr>
                        <td class="cart_description">
                            <h4><a href="">{{$item['name']}}</a></h4>
                            <p>Web ID: 1089772</p>
                        </td>
                        <td class="cart_price">
                            <p>${{$item['price']}}</p>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">{{$item['qty']}}</p>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">${{$total}}</p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                        </td>
                    </tr> 
                @endforeach
                  
        </tbody>
    </table> 
    <div>
        <td colspan="2">
            <table class="table table-condensed total-result">
                <tr>
                    <td>Cart Sub Total</td>
                    <td>${{$tong}}</td>
                </tr>
                <tr>
                    <td>Exo Tax</td>
                    <td>$0</td>
                </tr>
                <tr class="shipping-cost">
                    <td>Shipping Cost</td>
                    <td>Free</td>										
                </tr>
                <tr>
                    <td>Total</td>
                    <td><span>${{$tong}}</span></td>
                </tr>
            </table>
        </td>
    </div>     
</div>
        
