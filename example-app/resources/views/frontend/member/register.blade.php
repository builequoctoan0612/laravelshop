@extends('frontend.layouts.app')
@section('content')
@if (session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
            {{session('success')}}
        </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
        <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
        <ul>
            @foreach ($errors->all() as $er)
                <li>{{$er}}</li>
            @endforeach
            
        </ul>
    </div>
@endif
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
        
            <div class="col-sm-4">
                <div class="signup-form"><!--sign up form-->
                    <h2>New User Signup!</h2>
                    <form action="#" enctype="multipart/form-data" method="POST">
                        @csrf
                        <input type="text" placeholder="Name" name='name'/>
                        <input type="email" placeholder="Email Address" name='email'/>
                        <input type="password" placeholder="Password" name='password'/>
                        <input type="text" placeholder="Phone" name='phone'/>
                        <input type="text" placeholder="Address" name='address'/>
                        <input type="file" placeholder="Avatar"  name="avatar">
                        <select name="id_country">
                            @foreach ($country as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-default">Signup</button>
                    </form>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section>
@endsection