@extends('frontend.layouts.app')
@section('content')

    <div class="blog-post-area">
        @if (session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
            <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
            {{session('success')}}
        </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismis="alert" aria-hidden="true">x</button>
        <h4><i class="icon fa fa-check"></i>Thông Báo</h4>
        <ul>
            @foreach ($errors->all() as $er)
                <li>{{$er}}</li>
            @endforeach
            
        </ul>
    </div>
@endif
        <h2 class="title text-center">Update user</h2>
         <div class="signup-form"><!--sign up f"orm-->
        <h2>Edit User!</h2>
        <form action="" method="post" enctype="multipart/form-data">
            @csrf
            <input type="text" value="{{$user->name}}" name='name' />

            <input type="email" disabled value="{{$user->email}}" name="email" />
  
            <input type="password" placeholder="Password" name="password" />

            <input type="text" placeholder="Phone" value="{{$user->phone}}" name='phone' />

            <select class="form-control form-control-line" name="id_country">
                @foreach ($country as $item)
                    @if ($item->id==$user->id_country)
                        <option value="{{$item->id}}" selected>{{$item->name}}</option>                                           
                    @else
                        <option value="{{$item->id}}" >{{$item->name}}</option>                                                                                      
                    @endif                                    
                @endforeach
            </select>

            <input type="text" placeholder="Address" value="{{$user->address}}" name='address' />

            <input type="file" placeholder="" name='avatar' />

            <button type="submit" class="btn btn-default">SAVE</button>
        </form>
    </div>
    </div>

@endsection